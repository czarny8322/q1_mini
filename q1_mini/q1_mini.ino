//  -----               -----
// |  5  |             |  1  |
// | P07 |             | P02 |
//  ----- -----   ----- -----
//       |  6  | |  2  |
//       | P08 | | P03 |
//        -----   -----
//       |  7  | |  3  |
//       | P11 | | P05 |
//  ----- -----   ----- -----
// |  8  |             |  4  |
// | P16 |             | P15 |
//  -----               -----  (Top View)

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

#define MIN_PULSE_WIDTH 650 
#define MAX_PULSE_WIDTH 2350
#define DEFAULT_PULSE_WIDTH 1500
#define FREQUENCY 50

uint8_t servonum = 0;


const int numberOfServos = 8; // Number of servos
const int numberOfACE = 9; // Number of action code elements
int servoCal[] = { -50, 65, -30, 30, 25, -40, 60, -35 }; // Servo calibration data
//WARTOSCI: -50, 65, -30, 30, 25, -40, 60, -35, 
int servoPos[] = { 0, 0, 0, 0, 0, 0, 0, 0 }; // Servo current position
int servoPrgPeriod = 20; // 20 ms
int servo[numberOfServos]; // Servo object

void setup() 
{
Serial.begin(115200);
Serial.println("16 channel Servo test!");
pwm.begin();
pwm.setPWMFreq(FREQUENCY);
}


int pulseWidth(int angle)
{
int pulse_wide, analog_value;
pulse_wide = map(angle, 0, 180, MIN_PULSE_WIDTH, MAX_PULSE_WIDTH);
analog_value = int(float(pulse_wide) / 1000000 * FREQUENCY * 4096);
//Serial.println(analog_value);
return analog_value;
}


// Action code
// --------------------------------------------------------------------------------

// Servo zero position
// -------------------------- P02, P03, P05, P15, P07, P08, P11, P16
int servoAct00 [] PROGMEM = { 135,  45, 135,  45,  45, 135,  45, 135 };

// Zero
int servoPrg00step = 1;
int servoPrg00 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {  135,  45, 135,  45,  45, 135,  45, 135,  400  }, // zero position
};

// Standby
int servoPrg01step = 2;
int servoPrg01 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   90,  90,  90,  90,  90,  90,  90,  90,  200  }, // servo center point
  {   70,  90,  90, 110, 110,  90,  90,  70,  200  }, // standby
};

// Forward
int servoPrg02step = 11;
int servoPrg02 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   90,  90,  90, 110, 110,  90,  45,  90,  100  }, // leg1,4 up; leg4 fw
  {   70,  90,  90, 110, 110,  90,  45,  70,  100  }, // leg1,4 dn
  {   70,  90,  90,  90,  90,  90,  45,  70,  100  }, // leg2,3 up
  {   70,  45, 135,  90,  90,  90,  90,  70,  100  }, // leg1,4 bk; leg2 fw
  {   70,  45, 135, 110, 110,  90,  90,  70,  100  }, // leg2,3 dn
  {   90,  90, 135, 110, 110,  90,  90,  90,  100  }, // leg1,4 up; leg1 fw
  {   90,  90,  90, 110, 110, 135,  90,  90,  100  }, // leg2,3 bk
  {   70,  90,  90, 110, 110, 135,  90,  70,  100  }, // leg1,4 dn
  {   70,  90,  90, 110,  90, 135,  90,  70,  100  }, // leg3 up
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg3 fw dn
};

// Backward
int servoPrg03step = 11;
int servoPrg03 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   90,  45,  90, 110, 110,  90,  90,  90,  100  }, // leg4,1 up; leg1 fw
  {   70,  45,  90, 110, 110,  90,  90,  70,  100  }, // leg4,1 dn
  {   70,  45,  90,  90,  90,  90,  90,  70,  100  }, // leg3,2 up
  {   70,  90,  90,  90,  90, 135,  45,  70,  100  }, // leg4,1 bk; leg3 fw
  {   70,  90,  90, 110, 110, 135,  45,  70,  100  }, // leg3,2 dn
  {   90,  90,  90, 110, 110, 135,  90,  90,  100  }, // leg4,1 up; leg4 fw
  {   90,  90, 135, 110, 110,  90,  90,  90,  100  }, // leg3,1 bk
  {   70,  90, 135, 110, 110,  90,  90,  70,  100  }, // leg4,1 dn
  {   70,  90, 135,  90, 110,  90,  90,  70,  100  }, // leg2 up
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg2 fw dn
};

// Move Left
int servoPrg04step = 11;
int servoPrg04 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   70,  90,  45,  90,  90,  90,  90,  70,  100  }, // leg3,2 up; leg2 fw
  {   70,  90,  45, 110, 110,  90,  90,  70,  100  }, // leg3,2 dn
  {   90,  90,  45, 110, 110,  90,  90,  90,  100  }, // leg1,4 up
  {   90, 135,  90, 110, 110,  45,  90,  90,  100  }, // leg3,2 bk; leg1 fw
  {   70, 135,  90, 110, 110,  45,  90,  70,  100  }, // leg1,4 dn
  {   70, 135,  90,  90,  90,  90,  90,  70,  100  }, // leg3,2 up; leg3 fw
  {   70,  90,  90,  90,  90,  90, 135,  70,  100  }, // leg1,4 bk
  {   70,  90,  90, 110, 110,  90, 135,  70,  100  }, // leg3,2 dn
  {   70,  90,  90, 110, 110,  90, 135,  90,  100  }, // leg4 up
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg4 fw dn
};

// Move Right
int servoPrg05step = 11;
int servoPrg05 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   70,  90,  90,  90,  90,  45,  90,  70,  100  }, // leg2,3 up; leg3 fw
  {   70,  90,  90, 110, 110,  45,  90,  70,  100  }, // leg2,3 dn
  {   90,  90,  90, 110, 110,  45,  90,  90,  100  }, // leg4,1 up
  {   90,  90,  45, 110, 110,  90, 135,  90,  100  }, // leg2,3 bk; leg4 fw
  {   70,  90,  45, 110, 110,  90, 135,  70,  100  }, // leg4,1 dn
  {   70,  90,  90,  90,  90,  90, 135,  70,  100  }, // leg2,3 up; leg2 fw
  {   70, 135,  90,  90,  90,  90,  90,  70,  100  }, // leg4,1 bk
  {   70, 135,  90, 110, 110,  90,  90,  70,  100  }, // leg2,3 dn
  {   90, 135,  90, 110, 110,  90,  90,  70,  100  }, // leg1 up
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg1 fw dn
};

// Turn left
int servoPrg06step = 8;
int servoPrg06 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   90,  90,  90, 110, 110,  90,  90,  90,  100  }, // leg1,4 up
  {   90, 135,  90, 110, 110,  90, 135,  90,  100  }, // leg1,4 turn
  {   70, 135,  90, 110, 110,  90, 135,  70,  100  }, // leg1,4 dn
  {   70, 135,  90,  90,  90,  90, 135,  70,  100  }, // leg2,3 up
  {   70, 135, 135,  90,  90, 135, 135,  70,  100  }, // leg2,3 turn
  {   70, 135, 135, 110, 110, 135, 135,  70,  100  }, // leg2,3 dn
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg1,2,3,4 turn
};

// Turn right
int servoPrg07step = 8;
int servoPrg07 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   70,  90,  90,  90,  90,  90,  90,  70,  100  }, // leg2,3 up
  {   70,  90,  45,  90,  90,  45,  90,  70,  100  }, // leg2,3 turn
  {   70,  90,  45, 110, 110,  45,  90,  70,  100  }, // leg2,3 dn
  {   90,  90,  45, 110, 110,  45,  90,  90,  100  }, // leg1,4 up
  {   90,  45,  45, 110, 110,  45,  45,  90,  100  }, // leg1,4 turn
  {   70,  45,  45, 110, 110,  45,  45,  70,  100  }, // leg1,4 dn
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg1,2,3,4 turn
};

// Lie
int servoPrg08step = 1;
int servoPrg08 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {  110,  90,  90,  70,  70,  90,  90, 110,  500  }, // leg1,4 up
};

// Say Hi
int servoPrg09step = 4;
int servoPrg09 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {  120,  90,  90, 110,  60,  90,  90,  70,  200  }, // leg1, 3 down
  {   70,  90,  90, 110, 110,  90,  90,  70,  200  }, // standby
  {  120,  90,  90, 110,  60,  90,  90,  70,  200  }, // leg1, 3 down
  {   70,  90,  90, 110, 110,  90,  90,  70,  200  }, // standby
};

// Fighting
int servoPrg10step = 11;
int servoPrg10 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {  120,  90,  90, 110,  60,  90,  90,  70,  200  }, // leg1, 2 down
  {  120,  70,  70, 110,  60,  70,  70,  70,  200  }, // body turn left
  {  120, 110, 110, 110,  60, 110, 110,  70,  200  }, // body turn right
  {  120,  70,  70, 110,  60,  70,  70,  70,  200  }, // body turn left
  {  120, 110, 110, 110,  60, 110, 110,  70,  200  }, // body turn right
  {   70,  90,  90,  70, 110,  90,  90, 110,  200  }, // leg1, 2 up ; leg3, 4 down
  {   70,  70,  70,  70, 110,  70,  70, 110,  200  }, // body turn left
  {   70, 110, 110,  70, 110, 110, 110, 110,  200  }, // body turn right
  {   70,  70,  70,  70, 110,  70,  70, 110,  200  }, // body turn left
  {   70, 110, 110,  70, 110, 110, 110, 110,  200  }, // body turn right
  {   70,  90,  90,  70, 110,  90,  90, 110,  200  }  // leg1, 2 up ; leg3, 4 down
};

// Push up
int servoPrg11step = 11;
int servoPrg11 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  300  }, // start
  {  100,  90,  90,  80,  80,  90,  90, 100,  400  }, // down
  {   70,  90,  90, 110, 110,  90,  90,  70,  500  }, // up
  {  100,  90,  90,  80,  80,  90,  90, 100,  600  }, // down
  {   70,  90,  90, 110, 110,  90,  90,  70,  700  }, // up
  {  100,  90,  90,  80,  80,  90,  90, 100, 1300  }, // down
  {   70,  90,  90, 110, 110,  90,  90,  70, 1800  }, // up
  {  135,  90,  90,  45,  45,  90,  90, 135,  200  }, // fast down
  {   70,  90,  90,  45,  60,  90,  90, 135,  500  }, // leg1 up
  {   70,  90,  90,  45, 110,  90,  90, 135,  500  }, // leg2 up
  {   70,  90,  90, 110, 110,  90,  90,  70,  500  }  // leg3, leg4 up
};

// Sleep
int servoPrg12step = 2;
int servoPrg12 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   30,  90,  90, 150, 150,  90,  90,  30,  200  }, // leg1,4 dn
  {   30,  45, 135, 150, 150, 135,  45,  30,  200  }, // protect myself
};

// Dancing 1
int servoPrg13step = 10;
int servoPrg13 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   90,  90,  90,  90,  90,  90,  90,  90,  300  }, // leg1,2,3,4 up
  {   50,  90,  90,  90,  90,  90,  90,  90,  300  }, // leg1 dn
  {   90,  90,  90, 130,  90,  90,  90,  90,  300  }, // leg1 up; leg2 dn
  {   90,  90,  90,  90,  90,  90,  90,  50,  300  }, // leg2 up; leg4 dn
  {   90,  90,  90,  90, 130,  90,  90,  90,  300  }, // leg4 up; leg3 dn
  {   50,  90,  90,  90,  90,  90,  90,  90,  300  }, // leg3 up; leg1 dn
  {   90,  90,  90, 130,  90,  90,  90,  90,  300  }, // leg1 up; leg2 dn
  {   90,  90,  90,  90,  90,  90,  90,  50,  300  }, // leg2 up; leg4 dn
  {   90,  90,  90,  90, 130,  90,  90,  90,  300  }, // leg4 up; leg3 dn
  {   90,  90,  90,  90,  90,  90,  90,  90,  300  }, // leg3 up
};

// Dancing 2
int servoPrg14step = 9;
int servoPrg14 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   70,  45, 135, 110, 110, 135,  45,  70,  300  }, // leg1,2,3,4 two sides
  {  115,  45, 135,  65, 110, 135,  45,  70,  300  }, // leg1,2 up
  {   70,  45, 135, 110,  65, 135,  45, 115,  300  }, // leg1,2 dn; leg3,4 up
  {  115,  45, 135,  65, 110, 135,  45,  70,  300  }, // leg3,4 dn; leg1,2 up
  {   70,  45, 135, 110,  65, 135,  45, 115,  300  }, // leg1,2 dn; leg3,4 up
  {  115,  45, 135,  65, 110, 135,  45,  70,  300  }, // leg3,4 dn; leg1,2 up
  {   70,  45, 135, 110,  65, 135,  45, 115,  300  }, // leg1,2 dn; leg3,4 up
  {  115,  45, 135,  65, 110, 135,  45,  70,  300  }, // leg3,4 dn; leg1,2 up
  {   75,  45, 135, 105, 110, 135,  45,  70,  300  }, // leg1,2 dn
};

// Dancing 3
int servoPrg15step = 10;
int servoPrg15 [][numberOfACE] PROGMEM = {
  // P02, P03, P05, P15, P07, P08, P11, P16,  ms
  {   70,  45,  45, 110, 110, 135, 135,  70,  300  }, // leg1,2,3,4 bk
  {  110,  45,  45,  60,  70, 135, 135,  70,  300  }, // leg1,2,3 up
  {   70,  45,  45, 110, 110, 135, 135,  70,  300  }, // leg1,2,3 dn
  {  110,  45,  45, 110,  70, 135, 135, 120,  300  }, // leg1,3,4 up
  {   70,  45,  45, 110, 110, 135, 135,  70,  300  }, // leg1,3,4 dn
  {  110,  45,  45,  60,  70, 135, 135,  70,  300  }, // leg1,2,3 up
  {   70,  45,  45, 110, 110, 135, 135,  70,  300  }, // leg1,2,3 dn
  {  110,  45,  45, 110,  70, 135, 135, 120,  300  }, // leg1,3,4 up
  {   70,  45,  45, 110, 110, 135, 135,  70,  300  }, // leg1,3,4 dn
  {   70,  90,  90, 110, 110,  90,  90,  70,  300  }, // standby
};

// --------------------------------------------------------------------------------


void runServo(int servoPrg[][numberOfACE], int step, int speed )
{
  for (int i = 0; i < step; i++) { // Loop for step

   int servoLookingVal[] = { 0, 0, 0, 0, 0, 0, 0, 0 }; 
  int loopCount = 5;

  for (int k = 0; k < numberOfServos; k++){
      int x = servoPos[k];
      int y = servoPrg[i][k];
      servoLookingVal[k] = ((y-x)/loopCount);
      pwm.setPWM(k, 0, pulseWidth(servoPos[k] + servoCal[k])); 
    }
       
  for(int i = 0; i < loopCount ; i++){
    for (int k = 0; k < numberOfServos; k++){
      servoPos[k] = servoPos[k] + servoLookingVal[k];
      pwm.setPWM(k, 0, pulseWidth(servoPos[k] + servoCal[k]));
        delay(speed);
        
        Serial.print(servoPos[k] + servoCal[k]);
        Serial.print(", ");          
    }  
    Serial.print("\n");       
  }
  }
}


void loop() {

//runServo(servoPrg14, servoPrg14step, 8); // zero position

while (Serial.available() == 0);
//int val = Serial.parseInt(); //read int or parseFloat for ..float...
int val = Serial.read();
char servoVal;

switch (val) {
  case 'w':
        runServo(servoPrg02, servoPrg02step, 8);
    break;
    case 's':
        runServo(servoPrg03, servoPrg03step, 8);
    break;
    case 'a':
        runServo(servoPrg04, servoPrg04step, 8);
    break;
    case 'd':
        runServo(servoPrg02, servoPrg02step, 8);
    break;
    case 'e':
        runServo(servoPrg00, servoPrg00step, 8);
    break;
}


       
//while (Serial.available() == 0);
//int val = Serial.parseInt(); //read int or parseFloat for ..float...
////int val = Serial.read() - '0'
//int servoVal;
//
//switch (val) {
//  case 1:
//    Serial.print("wybrane servo: ");
//    Serial.print(val);
//    Serial.println("podaj wartosc: \n");
//     while(!Serial.available() ){}
//      servoVal = Serial.parseInt();
//      Serial.println("wybrana wartosc to: ");
//      Serial.print(servoVal);
//     servoCal[val-1] = servoVal;      
//    break;
//    
//  case 2:
//    Serial.println("wybrane servo: ");
//    Serial.print(val);
//    Serial.println("podaj wartosc: \n");
//     while(!Serial.available() ){}
//      servoVal = Serial.parseInt();
//      Serial.println("wybrana wartosc to: ");
//      Serial.println(servoVal);
//    servoCal[val-1] = servoVal;
//    break;
//case 3:
//    Serial.println("wybrane servo: ");
//    Serial.print(val);
//    Serial.println("podaj wartosc: \n");
//     while(!Serial.available() ){}
//      servoVal = Serial.parseInt();
//      Serial.println("wybrana wartosc to: ");
//      Serial.println(servoVal);
//     servoCal[val-1] = servoVal;      
//    break;
//    
//  case 4:
//    Serial.println("wybrane servo: ");
//    Serial.println(val);
//    Serial.println("podaj wartosc: \n");
//     while(!Serial.available() ){}
//      servoVal = Serial.parseInt();
//      Serial.println("wybrana wartosc to: ");
//      Serial.println(servoVal);
//    servoCal[val-1] = servoVal;
//    break;
//    case 5:
//    Serial.println("wybrane servo: ");
//    Serial.println(val);
//    Serial.println("podaj wartosc: \n");
//     while(!Serial.available() ){}
//      servoVal = Serial.parseInt();
//      Serial.println("wybrana wartosc to: ");
//      Serial.println(servoVal);
//     servoCal[val-1] = servoVal;      
//    break;
//    
//  case 6:
//    Serial.println("wybrane servo: ");
//    Serial.println(val);
//    Serial.println("podaj wartosc: \n");
//     while(!Serial.available() ){}
//      servoVal = Serial.parseInt();
//      Serial.println("wybrana wartosc to: ");
//      Serial.println(servoVal);
//    servoCal[val-1] = servoVal;
//    break;
//    case 7:
//    Serial.println("wybrane servo: ");
//    Serial.println(val);
//    Serial.println("podaj wartosc: \n");
//     while(!Serial.available() ){}
//      servoVal = Serial.parseInt();
//      Serial.println("wybrana wartosc to: ");
//      Serial.println(servoVal);
//     servoCal[val-1] = servoVal;      
//    break;
//    
//  case 8:
//    Serial.println("wybrane servo: ");
//    Serial.println(val);
//    Serial.println("podaj wartosc: \n");
//     while(!Serial.available() ){}
//      servoVal = Serial.parseInt();
//      Serial.println("wybrana wartosc to: ");
//      Serial.println(servoVal);
//    servoCal[val-1] = servoVal;
//    break;
//
//
//    
//  case 0:
//    Serial.print("WARTOSCI: ");
//      for (int x = 0; x < 8; x++) {
//          Serial.print(servoCal[x]);
//          Serial.print(", ");         
//        } 
//     Serial.print("\n");
//    break;
//}

}
