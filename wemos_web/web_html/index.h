const char MAIN_page[] PROGMEM = R"=====(
<html lang='fr'><head><meta http-equiv='refresh' content='60' name='viewport' content='width=device-width, initial-scale=1'/>
   <link href='bootstrap.min.css' rel='stylesheet'>
   <title>ESP8266 Demo - www.projetsdiy.fr</title></head><body>
   <div class='container-fluid'>
     <div class='row'>
       <div class='col-md-12'>
         <h1>Demo Webserver ESP8266 + Bootstrap</h1>
         <h3>Mini station m&eacutet&eacuteo</h3>
         <ul class='nav nav-pills'>
           <li class='active'>
             <a href='#'> <span class='badge pull-right'>
             t
             </span> Temp&eacuterature</a>
           </li><li>
             <a href='#'> <span class='badge pull-right'>
             h
             </span> Humidit&eacute</a>
           </li><li>
             <a href='#'> <span class='badge pull-right'>
             p
             </span> Pression atmosph&eacuterique</a></li>
         </ul>
         <table class='table'>  // Tableau des relevés
           <thead><tr><th>Capteur</th><th>Mesure</th><th>Valeur</th><th>Valeur pr&eacutec&eacutedente</th></tr></thead> //Entête
           <tbody>  // Contenu du tableau
             <tr><td>DHT22</td><td>Temp&eacuterature</td><td>
               t
               &degC</td><td>
               -</td></tr>
             <tr class='active'><td>DHT22</td><td>Humidit&eacute</td><td>
               h
               %</td><td>
               -</td></tr>
             <tr><td>BMP180</td><td>Pression atmosph&eacuterique</td><td>
               p
               mbar</td><td>
               -</td></tr>
         </tbody></table>
         <h3>GPIO</h3>
         <div class='row'>
           <div class='col-md-4'><h4 class ='text-left'>D5 
             <span class='badge'>
             etatGpio[0]
           </span></h4></div>
           <div class='col-md-4'><form action='/' method='POST'><button type='button submit' name='D5' value='1' class='btn btn-success btn-lg'>ON</button></form></div>
           <div class='col-md-4'><form action='/' method='POST'><button type='button submit' name='D5' value='0' class='btn btn-danger btn-lg'>OFF</button></form></div>
           <div class='col-md-4'><h4 class ='text-left'>D6 
             <span class='badge'>
             etatGpio[1]
           </span></h4></div>
           <div class='col-md-4'><form action='/' method='POST'><button type='button submit' name='D6' value='1' class='btn btn-success btn-lg'>ON</button></form></div>
           <div class='col-md-4'><form action='/' method='POST'><button type='button submit' name='D6' value='0' class='btn btn-danger btn-lg'>OFF</button></form></div>
           <div class='col-md-4'><h4 class ='text-left'>D7 
             <span class='badge'>
             etatGpio[2]
           </span></h4></div>
           <div class='col-md-4'><form action='/' method='POST'><button type='button submit' name='D7' value='1' class='btn btn-success btn-lg'>ON</button></form></div>
           <div class='col-md-4'><form action='/' method='POST'><button type='button submit' name='D7' value='0' class='btn btn-danger btn-lg'>OFF</button></form></div>
           <div class='col-md-4'><h4 class ='text-left'>D8 
             <span class='badge'>
             etatGpio[3]
           </span></h4></div>
           <div class='col-md-4'><form action='/' method='POST'><button type='button submit' name='D8' value='1' class='btn btn-success btn-lg'>ON</button></form></div>
           <div class='col-md-4'><form action='/' method='POST'><button type='button submit' name='D8' value='0' class='btn btn-danger btn-lg'>OFF</button></form></div>
         
		 <div class="input-group mb-3 col-md-2">
		 //<form action='/' method='POST'>
		  <input type="text" name="myTest" id="myTest" value="" class="form-control" placeholder="Set Value" aria-label="Set Value" aria-describedby="basic-addon2">
			<div class="input-group-append">
				<button onclick="sendData()" class="btn btn-outline-secondary" type="button">Button</button>
		   <label for="male" id="myInput">myVal</label>
		 //</form>
		 </div>
</div>
		 
</div>
		 
		 </div>
   </div></div></div>
   <script src='bootstrap.min.js'></script>
   
   
   
   <script>
		function sendData() {
		  var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					
					var x = document.getElementById("myTest").value;
					document.getElementById("myInput").innerHTML = x;
					
					  document.getElementById("LEDState").innerHTML =
					  this.responseText;
					}			  
			  
		  };
		  //xhttp.open("GET", "setLED?LEDstate="+led, true);
		  xhttp.send();
		}
		 
		setInterval(function() {
		  // Call a function repetatively with 2 Second interval
		  getData();
		}, 2000); //2000mSeconds update rate
		 
		function getData() {
		  var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
			  document.getElementById("ADCValue").innerHTML =
			  this.responseText;
			}
		  };
		  xhttp.open("GET", "readADC", true);
		  xhttp.send();
		}
</script>
   
   
   </body></html>
   )=====";