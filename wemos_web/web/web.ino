
//  -----               -----
// |  5  |             |  1  |
// | P05 |             | P01 |
//  ----- -----   ----- -----
//       |  6  | |  2  |
//       | P06 | | P02 |
//        -----   -----
//       |  7  | |  3  |
//       | P07 | | P03 |
//  ----- -----   ----- -----
// |  8  |             |  4  |
// | P08 |             | P04 |
//  -----               -----  (Top View)




#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

#include <FS.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include "index.h"

#define MIN_PULSE_WIDTH 650 
#define MAX_PULSE_WIDTH 2350
#define DEFAULT_PULSE_WIDTH 1500
#define FREQUENCY 50
uint8_t servonum = 0;

const int numberOfServos = 8; // Number of servos
const int numberOfACE = 9; // Number of action code elements
String servosName[] = {"P01", "P02", "P03", "P04", "P05", "P06", "P07", "P08"};
int servoCal[] = { -50, 65, -30, 30, 25, -40, 60, -35 }; // Servo calibration data
//WARTOSCI: -50, 65, -30, 30, 25, -40, 60, -35, 
int servoPos[] = { 0, 0, 0, 0, 0, 0, 0, 0 }; // Servo current position
int servoPrgPeriod = 20; // 20 ms
int servo[numberOfServos]; // Servo object
int servoSpeed = 4; // 1-10 lower = faster

#define ssid      "ESPap1"       // WiFi SSID
#define password  "adamadam"  // WiFi password

String myWebPage = MAIN_page;
String CalibrationPage = CALIBRATION_page;

bool calibrationSite = 0;
int currentSpiderCommand = 5; //STOP
bool currentProgramMode = 0;  //Single move

ESP8266WebServer server ( 80 );

/* Just a little test message.  Go to http://192.168.4.1 in a web browser
   connected to this access point to see it.
*/

void setup() {   
  delay(1000);
  Serial.begin(115200);
  Serial.println();
  Serial.print("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.on("/", handleRoot);
  server.on("/CALIBRATION", handleCalibration);  
  server.on("/setVAL", setDATA);
  server.on("/getData", getDATA);
  server.on("/resetCal", resetCalibration);
  server.on("/command", runCommand);  
  server.begin();
  
  server.on("/bootstrap.min.css", bootstrap);
  server.on("bootstrap.min.css", bootstrap);
  server.on("/bootstrap.min.js", bootstrapmin);
  server.on("bootstrap.min.js", bootstrapmin);
  SPIFFS.begin();
  
  Serial.println("HTTP server started");  

  pwm.begin();
  pwm.setPWMFreq(FREQUENCY);
}

// Action code
// --------------------------------------------------------------------------------

// Servo zero position
// -------------------------- P01, P02, P03, P04, P05, P06, P07, P08
int servoAct00 [] = { 135,  45, 135,  45,  45, 135,  45, 135 };

// Zero
int servoPrg00step = 1;
int servoPrg00 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {  135,  45, 135,  45,  45, 135,  45, 135,  400  }, // zero position
};

// Stay
int servoPrg16step = 1;
int servoPrg16 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  200  }, // servo center point  
};

// Standby
int servoPrg01step = 2;
int servoPrg01 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   90,  90,  90,  90,  90,  90,  90,  90,  200  }, // servo center point
  {   70,  90,  90, 110, 110,  90,  90,  70,  200  }, // standby
};

// Forward
int servoPrg02step = 11;
int servoPrg02 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   90,  90,  90, 110, 110,  90,  45,  90,  100  }, // leg1,4 up; leg4 fw
  {   70,  90,  90, 110, 110,  90,  45,  70,  100  }, // leg1,4 dn
  {   70,  90,  90,  90,  90,  90,  45,  70,  100  }, // leg2,3 up
  {   70,  45, 135,  90,  90,  90,  90,  70,  100  }, // leg1,4 bk; leg2 fw
  {   70,  45, 135, 110, 110,  90,  90,  70,  100  }, // leg2,3 dn
  {   90,  90, 135, 110, 110,  90,  90,  90,  100  }, // leg1,4 up; leg1 fw
  {   90,  90,  90, 110, 110, 135,  90,  90,  100  }, // leg2,3 bk
  {   70,  90,  90, 110, 110, 135,  90,  70,  100  }, // leg1,4 dn
  {   70,  90,  90, 110,  90, 135,  90,  70,  100  }, // leg3 up
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg3 fw dn
};

// Backward
int servoPrg03step = 11;
int servoPrg03 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   90,  45,  90, 110, 110,  90,  90,  90,  100  }, // leg4,1 up; leg1 fw
  {   70,  45,  90, 110, 110,  90,  90,  70,  100  }, // leg4,1 dn
  {   70,  45,  90,  90,  90,  90,  90,  70,  100  }, // leg3,2 up
  {   70,  90,  90,  90,  90, 135,  45,  70,  100  }, // leg4,1 bk; leg3 fw
  {   70,  90,  90, 110, 110, 135,  45,  70,  100  }, // leg3,2 dn
  {   90,  90,  90, 110, 110, 135,  90,  90,  100  }, // leg4,1 up; leg4 fw
  {   90,  90, 135, 110, 110,  90,  90,  90,  100  }, // leg3,1 bk
  {   70,  90, 135, 110, 110,  90,  90,  70,  100  }, // leg4,1 dn
  {   70,  90, 135,  90, 110,  90,  90,  70,  100  }, // leg2 up
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg2 fw dn
};

// Move Left
int servoPrg04step = 11;
int servoPrg04 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   70,  90,  45,  90,  90,  90,  90,  70,  100  }, // leg3,2 up; leg2 fw
  {   70,  90,  45, 110, 110,  90,  90,  70,  100  }, // leg3,2 dn
  {   90,  90,  45, 110, 110,  90,  90,  90,  100  }, // leg1,4 up
  {   90, 135,  90, 110, 110,  45,  90,  90,  100  }, // leg3,2 bk; leg1 fw
  {   70, 135,  90, 110, 110,  45,  90,  70,  100  }, // leg1,4 dn
  {   70, 135,  90,  90,  90,  90,  90,  70,  100  }, // leg3,2 up; leg3 fw
  {   70,  90,  90,  90,  90,  90, 135,  70,  100  }, // leg1,4 bk
  {   70,  90,  90, 110, 110,  90, 135,  70,  100  }, // leg3,2 dn
  {   70,  90,  90, 110, 110,  90, 135,  90,  100  }, // leg4 up
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg4 fw dn
};

// Move Right
int servoPrg05step = 11;
int servoPrg05 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   70,  90,  90,  90,  90,  45,  90,  70,  100  }, // leg2,3 up; leg3 fw
  {   70,  90,  90, 110, 110,  45,  90,  70,  100  }, // leg2,3 dn
  {   90,  90,  90, 110, 110,  45,  90,  90,  100  }, // leg4,1 up
  {   90,  90,  45, 110, 110,  90, 135,  90,  100  }, // leg2,3 bk; leg4 fw
  {   70,  90,  45, 110, 110,  90, 135,  70,  100  }, // leg4,1 dn
  {   70,  90,  90,  90,  90,  90, 135,  70,  100  }, // leg2,3 up; leg2 fw
  {   70, 135,  90,  90,  90,  90,  90,  70,  100  }, // leg4,1 bk
  {   70, 135,  90, 110, 110,  90,  90,  70,  100  }, // leg2,3 dn
  {   90, 135,  90, 110, 110,  90,  90,  70,  100  }, // leg1 up
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg1 fw dn
};

// Turn left
int servoPrg06step = 8;
int servoPrg06 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   90,  90,  90, 110, 110,  90,  90,  90,  100  }, // leg1,4 up
  {   90, 135,  90, 110, 110,  90, 135,  90,  100  }, // leg1,4 turn
  {   70, 135,  90, 110, 110,  90, 135,  70,  100  }, // leg1,4 dn
  {   70, 135,  90,  90,  90,  90, 135,  70,  100  }, // leg2,3 up
  {   70, 135, 135,  90,  90, 135, 135,  70,  100  }, // leg2,3 turn
  {   70, 135, 135, 110, 110, 135, 135,  70,  100  }, // leg2,3 dn
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg1,2,3,4 turn
};

// Turn right
int servoPrg07step = 8;
int servoPrg07 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // standby
  {   70,  90,  90,  90,  90,  90,  90,  70,  100  }, // leg2,3 up
  {   70,  90,  45,  90,  90,  45,  90,  70,  100  }, // leg2,3 turn
  {   70,  90,  45, 110, 110,  45,  90,  70,  100  }, // leg2,3 dn
  {   90,  90,  45, 110, 110,  45,  90,  90,  100  }, // leg1,4 up
  {   90,  45,  45, 110, 110,  45,  45,  90,  100  }, // leg1,4 turn
  {   70,  45,  45, 110, 110,  45,  45,  70,  100  }, // leg1,4 dn
  {   70,  90,  90, 110, 110,  90,  90,  70,  100  }, // leg1,2,3,4 turn
};

// Lie
int servoPrg08step = 1;
int servoPrg08 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {  110,  90,  90,  70,  70,  90,  90, 110,  500  }, // leg1,4 up
};

// Say Hi
int servoPrg09step = 4;
int servoPrg09 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {  120,  90,  90, 110,  60,  90,  90,  70,  200  }, // leg1, 3 down
  {   70,  90,  90, 110, 110,  90,  90,  70,  200  }, // standby
  {  120,  90,  90, 110,  60,  90,  90,  70,  200  }, // leg1, 3 down
  {   70,  90,  90, 110, 110,  90,  90,  70,  200  }, // standby
};

// Fighting
int servoPrg10step = 11;
int servoPrg10 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {  120,  90,  90, 110,  60,  90,  90,  70,  200  }, // leg1, 2 down
  {  120,  70,  70, 110,  60,  70,  70,  70,  200  }, // body turn left
  {  120, 110, 110, 110,  60, 110, 110,  70,  200  }, // body turn right
  {  120,  70,  70, 110,  60,  70,  70,  70,  200  }, // body turn left
  {  120, 110, 110, 110,  60, 110, 110,  70,  200  }, // body turn right
  {   70,  90,  90,  70, 110,  90,  90, 110,  200  }, // leg1, 2 up ; leg3, 4 down
  {   70,  70,  70,  70, 110,  70,  70, 110,  200  }, // body turn left
  {   70, 110, 110,  70, 110, 110, 110, 110,  200  }, // body turn right
  {   70,  70,  70,  70, 110,  70,  70, 110,  200  }, // body turn left
  {   70, 110, 110,  70, 110, 110, 110, 110,  200  }, // body turn right
  {   70,  90,  90,  70, 110,  90,  90, 110,  200  }  // leg1, 2 up ; leg3, 4 down
};

// Push up
int servoPrg11step = 11;
int servoPrg11 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   70,  90,  90, 110, 110,  90,  90,  70,  300  }, // start
  {  100,  90,  90,  80,  80,  90,  90, 100,  400  }, // down
  {   70,  90,  90, 110, 110,  90,  90,  70,  500  }, // up
  {  100,  90,  90,  80,  80,  90,  90, 100,  600  }, // down
  {   70,  90,  90, 110, 110,  90,  90,  70,  700  }, // up
  {  100,  90,  90,  80,  80,  90,  90, 100, 1300  }, // down
  {   70,  90,  90, 110, 110,  90,  90,  70, 1800  }, // up
  {  135,  90,  90,  45,  45,  90,  90, 135,  200  }, // fast down
  {   70,  90,  90,  45,  60,  90,  90, 135,  500  }, // leg1 up
  {   70,  90,  90,  45, 110,  90,  90, 135,  500  }, // leg2 up
  {   70,  90,  90, 110, 110,  90,  90,  70,  500  }  // leg3, leg4 up
};

// Sleep
int servoPrg12step = 2;
int servoPrg12 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   30,  90,  90, 150, 150,  90,  90,  30,  200  }, // leg1,4 dn
  {   30,  45, 135, 150, 150, 135,  45,  30,  200  }, // protect myself
};

// Dancing 1
int servoPrg13step = 10;
int servoPrg13 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   90,  90,  90,  90,  90,  90,  90,  90,  300  }, // leg1,2,3,4 up
  {   50,  90,  90,  90,  90,  90,  90,  90,  300  }, // leg1 dn
  {   90,  90,  90, 130,  90,  90,  90,  90,  300  }, // leg1 up; leg2 dn
  {   90,  90,  90,  90,  90,  90,  90,  50,  300  }, // leg2 up; leg4 dn
  {   90,  90,  90,  90, 130,  90,  90,  90,  300  }, // leg4 up; leg3 dn
  {   50,  90,  90,  90,  90,  90,  90,  90,  300  }, // leg3 up; leg1 dn
  {   90,  90,  90, 130,  90,  90,  90,  90,  300  }, // leg1 up; leg2 dn
  {   90,  90,  90,  90,  90,  90,  90,  50,  300  }, // leg2 up; leg4 dn
  {   90,  90,  90,  90, 130,  90,  90,  90,  300  }, // leg4 up; leg3 dn
  {   90,  90,  90,  90,  90,  90,  90,  90,  300  }, // leg3 up
};

// Dancing 2
int servoPrg14step = 9;
int servoPrg14 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   70,  45, 135, 110, 110, 135,  45,  70,  300  }, // leg1,2,3,4 two sides
  {  115,  45, 135,  65, 110, 135,  45,  70,  300  }, // leg1,2 up
  {   70,  45, 135, 110,  65, 135,  45, 115,  300  }, // leg1,2 dn; leg3,4 up
  {  115,  45, 135,  65, 110, 135,  45,  70,  300  }, // leg3,4 dn; leg1,2 up
  {   70,  45, 135, 110,  65, 135,  45, 115,  300  }, // leg1,2 dn; leg3,4 up
  {  115,  45, 135,  65, 110, 135,  45,  70,  300  }, // leg3,4 dn; leg1,2 up
  {   70,  45, 135, 110,  65, 135,  45, 115,  300  }, // leg1,2 dn; leg3,4 up
  {  115,  45, 135,  65, 110, 135,  45,  70,  300  }, // leg3,4 dn; leg1,2 up
  {   75,  45, 135, 105, 110, 135,  45,  70,  300  }, // leg1,2 dn
};

// Dancing 3
int servoPrg15step = 10;
int servoPrg15 [][numberOfACE] = {
  // P01, P02, P03, P04, P05, P06, P07, P08,  ms
  {   70,  45,  45, 110, 110, 135, 135,  70,  300  }, // leg1,2,3,4 bk
  {  110,  45,  45,  60,  70, 135, 135,  70,  300  }, // leg1,2,3 up
  {   70,  45,  45, 110, 110, 135, 135,  70,  300  }, // leg1,2,3 dn
  {  110,  45,  45, 110,  70, 135, 135, 120,  300  }, // leg1,3,4 up
  {   70,  45,  45, 110, 110, 135, 135,  70,  300  }, // leg1,3,4 dn
  {  110,  45,  45,  60,  70, 135, 135,  70,  300  }, // leg1,2,3 up
  {   70,  45,  45, 110, 110, 135, 135,  70,  300  }, // leg1,2,3 dn
  {  110,  45,  45, 110,  70, 135, 135, 120,  300  }, // leg1,3,4 up
  {   70,  45,  45, 110, 110, 135, 135,  70,  300  }, // leg1,3,4 dn
  {   70,  90,  90, 110, 110,  90,  90,  70,  300  }, // standby
};

int pulseWidth(int angle)
{
  int pulse_wide, analog_value;
  pulse_wide = map(angle, 0, 180, MIN_PULSE_WIDTH, MAX_PULSE_WIDTH);
  analog_value = int(float(pulse_wide) / 1000000 * FREQUENCY * 4096);
  //Serial.println(analog_value);
  return analog_value;
}

void runServo(int servoPrg[][numberOfACE], int step, int speed )
{
  for (int i = 0; i < step; i++) { // Loop for step

   int servoLookingVal[] = { 0, 0, 0, 0, 0, 0, 0, 0 }; 
  int loopCount = 5;

  for (int k = 0; k < numberOfServos; k++){
      int x = servoPos[k];
      int y = servoPrg[i][k];
      servoLookingVal[k] = ((y-x)/loopCount);
      pwm.setPWM(k, 0, pulseWidth(servoPos[k] + servoCal[k])); 
    }
       
  for(int i = 0; i < loopCount ; i++){
    for (int k = 0; k < numberOfServos; k++){
      servoPos[k] = servoPos[k] + servoLookingVal[k];
      pwm.setPWM(k, 0, pulseWidth(servoPos[k] + servoCal[k]));
        delay(speed);
        
        Serial.print(servoPos[k] + servoCal[k]);
        Serial.print(", ");          
    }  
    Serial.print("\n");       
  }
  }
}

// --------------------------------------------------------------------------------



// ESP communication methods
// --------------------------------------------------------------------------------

void handleCalibration(){ 
   server.send ( 200, "text/html", CalibrationPage ); 
   calibrationSite = 1;
}
  
void handleRoot(){
    server.send ( 200, "text/html", myWebPage );
    calibrationSite = 0;
}

 void runCommand(){

  String program = server.arg("prog");
  String programMode = server.arg("mode");
  if (programMode == "true"){
    currentProgramMode = 1; //Single move
  }else{
    currentProgramMode = 0; //Continuous move
  }
  
  currentSpiderCommand = program.toInt();
  server.send ( 200, "text/html", program );
}


void runSpiderBoot(int command, bool prgMode){
  
switch (command) {
  case 1:
    // Sleep    
    runServo(servoPrg01, servoPrg01step, servoSpeed); 
    break;
  case 2:
    // UP      
    runServo(servoPrg02, servoPrg02step, servoSpeed);    
    break;
  case 3:
    // Say Hi
    runServo(servoPrg09, servoPrg09step, servoSpeed);
    break;
  case 4:
    // LEFT
    runServo(servoPrg04, servoPrg04step, servoSpeed);
    break;
  case 5:
    // STOP
    runServo(servoPrg16, servoPrg16step, servoSpeed);
    break;
  case 6:
    // RIGHT
    runServo(servoPrg02, servoPrg02step, servoSpeed);
    break;
  case 7:
    // Fighting
    runServo(servoPrg10, servoPrg10step, servoSpeed);
    break;
  case 8:
    // DOWN
    runServo(servoPrg03, servoPrg03step, servoSpeed);
    break;
  case 9:
    // Push up
    runServo(servoPrg11, servoPrg11step, servoSpeed);
    break;
  case 10:
    // Dancing 1
    runServo(servoPrg13, servoPrg13step, servoSpeed);
    break;
  case 11:
    // Dancing 2
    runServo(servoPrg14, servoPrg14step, servoSpeed);
    break;
  case 12:
    // Dancing 3
    runServo(servoPrg15, servoPrg15step, servoSpeed);
    break;
  default:
    // STOP
    runServo(servoPrg00, servoPrg00step, servoSpeed);
    break;
}

  if(currentProgramMode){
    currentSpiderCommand = 5;
  }
}



///----- FOR CALIBRATION PAGE -----/////
void setDATA() { 
 String servoName = server.arg("srvNum");
 String calibrationVal = server.arg("calVal");

 for (int i = 0; i < numberOfServos; i++) {
   if (servosName[i] == servoName){
      servoCal[i] = calibrationVal.toInt();      
      server.send(200, "text/plane", calibrationVal); //Send web page

      pwm.setPWM(i, 0, pulseWidth(servoAct00[i] + servoCal[i]));
   }
 }
}

void resetCalibration(){
  String srvCal= ""; 
  String strVal = "";   
  for (int i = 0; i < numberOfServos; i++) {      
      strVal = String(servoCal[i]);    
      srvCal += strVal+",";      
  }
  
  for (int i = 0; i < numberOfServos; i++) { 
    pwm.setPWM(i, 0, pulseWidth(servoAct00[i] + servoCal[i]));
    delay(100);
  }   
  server.send(200, "text/plane", srvCal); //Send web page 
}


void getDATA(){  
  String srvCal= ""; 
  String strVal = "";   
  for (int i = 0; i < numberOfServos; i++) {      
      strVal = String(servoCal[i]);    
      srvCal += strVal+",";      
  }
  server.send(200, "text/plane", srvCal); //Send web page
}

///----- FOR CALIBRATION PAGE -----/////



void bootstrap()
{
  File file = SPIFFS.open("/bootstrap.min.css", "r");
  size_t sent = server.streamFile(file, "text/css");
}
void bootstrapmin()
{
  File file = SPIFFS.open("/bootstrap.min.js", "r");
  size_t sent = server.streamFile(file, "application/javascript");
}


// --------------------------------------------------------------------------------


void loop() {  
  server.handleClient();
  //delay(1000);

  if(!calibrationSite)
  runSpiderBoot(currentSpiderCommand, currentProgramMode);
  
}
