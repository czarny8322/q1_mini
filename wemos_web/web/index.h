const char MAIN_page[] PROGMEM = R"=====(

<html lang='pl'>
   <head>      
      <link href='bootstrap.min.css' rel='stylesheet'>
	  <script src='bootstrap.min.js'></script>
      <title>My Spider Boot</title>	  
	  
<style>
    .btn-squared-default
    {
        width: 100px !important;
        height: 100px !important;
        font-size: 10px;
    }

        .btn-squared-default:hover
        {
            border: 3px solid white;
            font-weight: 800;
        }

    .btn-squared-default-plain
    {
        width: 100px !important;
        height: 100px !important;
        font-size: 10px;
    }

        .btn-squared-default-plain:hover
        {
            border: 0px solid white;
        }
</style>


   </head>
   <body>
   
<center><h1>My Spider Boot</h1></center>
<center><h4>Home page</h4></center>
   
      <div class='container-fluid'>
         <div class='row'>
            <div class='col-md-12'>
               <br/>
               <br/>
               <div class="row">
                  <div class="col-sm-6">
                     <form action="/">
                        <input type="submit" value="HOME" class="btn btn-success btn-lg btn-block"/>	
                     </form>
                  </div>
                  <div class="col-sm-6">
                     <form action="/CALIBRATION">
                        <input type="submit" value="CALIBRATION" class="btn btn-danger btn-lg btn-block"/>	
                     </form>
                  </div>
               </div>
               <br/>
			   
			   <div class="row">
                  <div class="col-xl"></div>
						 <table class="table">  
							<tr>
							<td colspan="3">
							<label style="margin-right: 10px">Working mode: </label>
							<label class="checkbox-inline"><input id="PrgMode" type="checkbox" value="1" checked>Single</label>							 
						</td>
							</tr>
							<tr>
								<td><button class="btn btn-squared-default btn-outline-secondary" type="button" onclick="sendCommand('1')">Standby</button></td>
								<td><button class="btn btn-squared-default btn-primary" type="button" onclick="sendCommand('2')">UP</button></td>
								<td><button class="btn btn-squared-default btn-outline-secondary" type="button" onclick="sendCommand('3')">Say Hi</button></td>
							</tr>						
							<tr>
								<td><button class="btn btn-squared-default btn-primary" type="button" onclick="sendCommand('4')">LEFT</button></td>
								<td><button class="btn btn-squared-default btn-danger" type="button" onclick="sendCommand('5')">STOP</button></td>
								<td><button class="btn btn-squared-default btn-primary" type="button" onclick="sendCommand('6')">RIGHT</button></td>
							</tr>
								<td><button class="btn btn-squared-default btn-outline-secondary" type="button" onclick="sendCommand('7')">Fighting</button></td>
								<td><button class="btn btn-squared-default btn-primary" type="button" onclick="sendCommand('8')">DOWN</button></td>
								<td><button class="btn btn-squared-default btn-outline-secondary" type="button" onclick="sendCommand('9')">Push up</button></td>
							<tr>
							</tr>
								<td></td>
								<td></td>
								<td></td>
							<tr>
							</tr>
								<td><button class="btn btn-squared-default btn-success" type="button" onclick="sendCommand('10')">Dancing 1</button></td>
								<td><button class="btn btn-squared-default btn-success" type="button" onclick="sendCommand('11')">Dancing 2</button></td>
								<td><button class="btn btn-squared-default btn-success" type="button" onclick="sendCommand('12')">Dancing 3</button></td>
							<tr>
				
							</tr> 						
						  </tbody>
						</table>
					  </div>
				  
               </div>
</table>  

<label id="Winput">LOG: </label> <label id="Winput"></label>


<script>
		function sendCommand(val) {
         	 var program = val;
			 var mode = document.getElementById("PrgMode").checked;
         		
         	  var xhttp = new XMLHttpRequest();
         	  xhttp.onreadystatechange = function() {
         		if (this.readyState == 4 && this.status == 200) {
         		  document.getElementById("Winput").innerHTML = this.responseText;
         		}
         	  };
         	  xhttp.open("GET", "command?prog="+program+"&mode="+mode, true);
         	  xhttp.send();
         	}			
</script>
<style>
.table {
   margin: auto;
   width: 50% !important; 
}
</style>
</body>
</html>
)=====";


const char CALIBRATION_page[] PROGMEM = R"=====(
<html lang='pl'>

<head>
    <link href='bootstrap.min.css' rel='stylesheet'>
    <script src='bootstrap.min.js'></script>
    <title>My Spider Boot</title>
</head>

<body onload="getData()">

    
        <center><h1>My Spider Boot</h1></center>
		<center><h4>Calibration Settings</h4></center>
    <div class='container-fluid'>
        <div class='row'>
            <div class='col-md-12'>
                <br/>
                <br/>

                <div class="row">
                    <div class="col-sm-6">
                        <form action="/">
                            <input type="submit" value="HOME" class="btn btn-success btn-lg btn-block" />
                        </form>
                    </div>

                    <div class="col-sm-6">
                        <form action="/CALIBRATION">
                            <input type="submit" value="CALIBRATION" class="btn btn-danger btn-lg btn-block" />
                        </form>
                    </div>
                </div>

                <br/>

                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Servo Name</th>
                            <th scope="col">P01</th>
                            <th scope="col">P02</th>
                            <th scope="col">P03</th>
                            <th scope="col">P04</th>
                            <th scope="col">P05</th>
                            <th scope="col">P06</th>
                            <th scope="col">P07</th>
                            <th scope="col">P08</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">#</th>
                            <td id="input_P01"></td>
                            <td id="input_P02"></td>
                            <td id="input_P03"></td>
                            <td id="input_P04"></td>
                            <td id="input_P05"></td>
                            <td id="input_P06"></td>
                            <td id="input_P07"></td>
                            <td id="input_P08"></td>
                        </tr>
                    </tbody>
                </table>
<!--
                <div class="container">
                    <div class="row">
                        <div class="input-group col">
                            <label>P05 (5)</label>
                            <input id="P05" type="text" class="form-control">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P05')">SET</button>
                            </div>
                        </div>
                        <div class="input-group col">
                            <label>P01 (1)</label>
                            <input id="P01" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P01')">SET</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="input-group col">
                            <label>P06 (6)</label>
                            <input id="P06" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P06')">SET</button>
                            </div>
                        </div>
                        <div class="input-group col">
                            <label>P02 (2)</label>
                            <input id="P02" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P02')">SET</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="input-group col">
                            <label>P07 (7)</label>
                            <input id="P07" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P07')">SET</button>
                            </div>
                        </div>
                        <div class="input-group col">
                            <label>P03 (3)</label>
                            <input id="P03" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P03')">SET</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="input-group col">
                            <label>P08 (8)</label>
                            <input id="P08" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P08')">SET</button>
                            </div>
                        </div>
                        <div class="input-group col">
                            <label>P04 (4)</label>
                            <input id="P04" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P04')">SET</button>
                            </div>
                        </div>
                    </div>
                </div>
                <center>
                    <h2>Top View</h2>
                </center>
-->
                <table class="table">
                    <tr>
                        <td class="table-primary">
                            <div class="input-group col">
                                <label id="label_P05">P05 (5)</label>
                                <input id="P05" type="text" class="form-control">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P05')">SET</button>
                                </div>
                            </div>
                        </td>

                        <td>
                            <div class="input-group col">
                        </td>
                        <td></td>
                        <td></td>
                        <td class="table-primary"> 
                            <div class="input-group col">
                                <label id="label_P01">P01 (1)</label>
                                <input id="P01" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P01')">SET</button>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td class="table-primary">
                            <div class="input-group col">
                                <label id="label_P06">P06 (8)</label>
                                <input id="P06" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P06')">SET</button>
                                </div>
                            </div>
                        </td>
                        <td></td>
                        <td class="table-primary">
                            <div class="input-group col">
                                <label id="label_P02">P02 (2)</label>
                                <input id="P02" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P02')">SET</button>
                                </div>
                            </div>
                        </td>
                        <td></td>
                    </tr>

                    <tr>

                        <td></td>
                        <td></td>
                        <td>
							<button class="btn btn-danger" type="button" onclick="resetCalibration()">RESET</button>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td class="table-primary">
                            <div class="input-group col">
                                <label id="label_P07">P07 (7)</label>
                                <input id="P07" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P07')">SET</button>
                                </div>
                            </div>
                        </td>
                        <td></td>
                        <td class="table-primary">
                            <div class="input-group col">
                                <label id="label_P03">P03 (3)</label>
                                <input id="P03" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P03')">SET</button>
                                </div>
                            </div>
                        </td>
                        <td></td>
                    </tr>

                    <tr>
                        <td class="table-primary">
                            <div class="input-group col">
                                <label id="label_P08">P08 (8)</label>
                                <input id="P08" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P08')">SET</button>
                                </div>
                            </div>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="table-primary">
                            <div class="input-group col">
                                <label id="label_P04">P04 (4)</label>
                                <input id="P04" type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" onclick="setDATA('P04')">SET</button>
                                </div>
                            </div>
                        </td>

                    </tr>
                    </tbody>
                </table>

                </div>
            </div>
        </div>

        <script>
            var servos = ["P01", "P02", "P03", "P04", "P05", "P06", "P07", "P08"];

            function setDATA(val) {
                var servoNumber = val;
                var calibrationVal = document.getElementById(val).value;

                //document.getElementById("P04s").innerHTML = this.responseText;
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("input_" + servoNumber).innerHTML = this.responseText;
						document.getElementById("label_" + servoNumber).innerHTML = this.responseText;
                    }
                };
                xhttp.open("GET", "setVAL?srvNum=" + servoNumber + "&calVal=" + calibrationVal, true);
                xhttp.send();
            }

            function getData() {
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var res = this.responseText;
                        var resArr = res.split(',');

                        for (i = 0; i < 8; i++) {
                            document.getElementById("input_" + servos[i]).innerHTML = resArr[i];
							document.getElementById("label_" + servos[i]).innerHTML = resArr[i];
                        }
                    }
                };
                xhttp.open("GET", "getData", true);
                xhttp.send();
            }
			
			
			function resetCalibration() {
				var xhttp = new XMLHttpRequest();
					xhttp.onreadystatechange = function() {
						if (this.readyState == 4 && this.status == 200) {
							var res = this.responseText;
							var resArr = res.split(',');

							for (i = 0; i < 8; i++) {
								document.getElementById("input_" + servos[i]).innerHTML = resArr[i];
							}
						}
					};
					xhttp.open("GET", "resetCal", true);
					xhttp.send();
			}
        </script>
</body>

</html>
)=====";